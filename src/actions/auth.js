import { normalize } from 'normalizr';
import * as schema from './schema';
import * as api from '../api';
import { getIsAuthActionLoading } from '../reducers';
import { saveAuthState, removeAuthState } from '../localStorage';

// Action creators
export const loginAttempt = () => ({ type: 'AUTH_LOGIN_ATTEMPT', actionName: 'login' });
export const loginFailure = error => ({ type: 'AUTH_LOGIN_FAILURE', actionName: 'login', error });
export const loginSuccess = data => ({ type: 'AUTH_LOGIN_SUCCESS', actionName: 'login', data });

export const logoutAttempt = () => ({ type: 'AUTH_LOGOUT_ATTEMPT', actionName: 'logout' });
export const logoutFailure = error => ({ type: 'AUTH_LOGOUT_FAILURE', actionName: 'logout', error });
export const logoutSuccess = () => ({ type: 'AUTH_LOGOUT_SUCCESS', actionName: 'logout' });

export const registrationAttempt = () => ({ type: 'AUTH_REGISTRATION_ATTEMPT', actionName: 'registration' });
export const registrationFailure = error => ({ type: 'AUTH_REGISTRATION_FAILURE', actionName: 'registration', error });
export const registrationSuccess = () => ({ type: 'AUTH_REGISTRATION_SUCCESS', actionName: 'registration' });

export const validateUserAttempt = () => ({ type: 'AUTH_VALIDATE_USER_ATTEMPT', actionName: 'validateUser' });
export const validateUserFailure = error => ({ type: 'AUTH_VALIDATE_USER_FAILURE', actionName: 'validateUser', error });
export const validateUserSuccess = data => ({ type: 'AUTH_VALIDATE_USER_SUCCESS', actionName: 'validateUser', data });

export const getUserAttempt = () => ({ type: 'AUTH_GETUSER_ATTEMPT', actionName: 'getUser' });
export const getUserFailure = error => ({ type: 'AUTH_GETUSER_FAILURE', actionName: 'getUser', error });
export const getUserSuccess = data => ({ type: 'AUTH_GETUSER_SUCCESS', actionName: 'getUser', data });

const handleToken = ({ success, token, user }, action) => {
  if (success) {
    if (action === 'logout') {
      api.removeApiAuthHeader();
      removeAuthState();
    } else if (token && (action === 'login' || action === 'registration')) {
      const userInfo = normalize(user, schema.user);
      saveAuthState({
        auth: {
          isAuthenticated: success,
          authToken: token,
          userInfo: userInfo.entities.users[userInfo.result],
        },
      });
      api.setApiAuthHeader(token);
    }
  }
};

export const login = (data = {}) => async (dispatch, getState) => {
  if (getIsAuthActionLoading(getState(), 'login')) {
    return Promise.resolve();
  }

  dispatch(loginAttempt());

  const response = await api.login(data);

  if (response.success) {
    handleToken(response, 'login');
    const token = response.token || null;
    dispatch(loginSuccess({
      response: normalize(response.user, schema.user),
      token,
    }));
  } else {
    if (response.error) {
      if (response.error.statusCode === 401 || response.error.status === 401) {
        handleToken({ success: true }, 'logout');
        dispatch(logoutSuccess());
      }
    }

    dispatch(loginFailure({
      message: response.error ?
        response.error.message || 'Something went wrong.' :
        'Something went wrong.',
      status: response.error ?
        response.error.status || 500 :
        500,
    }));
  }

  return response.success;
};

export const register = (data = {}) => async (dispatch, getState) => {
  if (getIsAuthActionLoading(getState(), 'registration')) {
    return Promise.resolve();
  }

  dispatch(registrationAttempt());

  const response = await api.register(data);

  if (response.success) {
    handleToken(response, 'registration');
    const token = response.token || null;
    await dispatch(loginSuccess({
      response: normalize(response.user, schema.user),
      token,
    }));
    await dispatch(registrationSuccess());
  } else {
    dispatch(registrationFailure({
      message: response.error ?
        response.error.message || 'Something went wrong.' :
        'Something went wrong.',
      status: response.error ?
        response.error.status || 500 :
        500,
    }));
  }

  return response.success;
};

export const getUser = (id, token = undefined) => async (dispatch, getState) => {
  if (getIsAuthActionLoading(getState(), 'getUser')) {
    return Promise.resolve();
  }

  dispatch(getUserAttempt());

  const response = await api.getUser(id);

  if (response.success) {
    dispatch(getUserSuccess({
      response: normalize(response.user, schema.user),
      token: token !== undefined ? token : undefined,
    }));
  } else {
    if (response.error) {
      if (response.error.statusCode === 401 || response.error.status === 401) {
        handleToken({ success: true }, 'logout');
        dispatch(logoutSuccess());
      }
    }

    dispatch(getUserFailure({
      message: response.error ?
        response.error.message || 'Something went wrong.' :
        'Something went wrong.',
      status: response.error ?
        response.error.status || 500 :
        500,
    }));
  }

  return response.success;
};

export const logout = () => async (dispatch, getState) => {
  if (getIsAuthActionLoading(getState(), 'logout')) {
    return Promise.resolve();
  }

  dispatch(logoutAttempt());

  const response = await api.logout();

  if (response.success) {
    handleToken(response, 'logout');
    dispatch(logoutSuccess());
  } else {
    if (response.error) {
      if (response.error.statusCode === 401 || response.error.status === 401) {
        handleToken({ success: true }, 'logout');
        return dispatch(logoutSuccess());
      }
    }

    dispatch(logoutFailure({
      message: response.error ?
        response.error.message || 'Something went wrong.' :
        'Something went wrong.',
      status: response.error ?
        response.error.status || 500 :
        500,
    }));
  }

  return response.success;
};

export const validateUser = (data = {}) => async (dispatch, getState) => {
  if (getIsAuthActionLoading(getState(), 'validateUser')) {
    return Promise.resolve();
  }

  dispatch(validateUserAttempt());

  const response = await api.validateUser(data);

  if (response.success) {
    dispatch(validateUserSuccess({
      response,
    }));

    return response;
  }

  dispatch(validateUserFailure({
    message: response.error ?
      response.error.message || 'Something went wrong.' :
      'Something went wrong.',
    status: response.error ?
      response.error.status || 500 :
      500,
  }));

  return { success: false, result: false };
};
