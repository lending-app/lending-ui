import { normalize } from 'normalizr';
import * as schema from './schema';
import * as api from '../api';
import { getIsChecksActionLoading } from '../reducers';

// Actions creators
export const clearUserChecks = () => ({ type: 'CHECKS_CLEAR' });

export const getUserCheckAttempt = () => ({ type: 'CHECKS_GET_ATTEMPT', actionName: 'get' });
export const getUserCheckFailure = error => ({ type: 'CHECKS_GET_FAILURE', actionName: 'get', error });
export const getUserCheckSuccess = data => ({ type: 'CHECKS_GET_SUCCESS', actionName: 'get', data });

export const createUserCheckAttempt = () => ({ type: 'CHECKS_CREATE_ATTEMPT', actionName: 'create' });
export const createUserCheckFailure = error => ({ type: 'CHECKS_CREATE_FAILURE', actionName: 'create', error });
export const createUserCheckSuccess = data => ({ type: 'CHECKS_CREATE_SUCCESS', actionName: 'create', data });

export const uploadUserCheckFilesAttempt = () => ({ type: 'CHECKS_UPLOAD_ATTEMPT', actionName: 'upload' });
export const uploadUserCheckFilesFailure = error => ({ type: 'CHECKS_UPLOAD_FAILURE', actionName: 'upload', error });
export const uploadUserCheckFilesSuccess = data => ({ type: 'CHECKS_UPLOAD_SUCCESS', actionName: 'upload', data });

export const getUserCheck = (userId, checkId = null) => async (dispatch, getState) => {
  if (getIsChecksActionLoading(getState(), 'get')) {
    return Promise.resolve();
  }

  dispatch(getUserCheckAttempt());

  const response = await api.getUserCheck(userId, checkId);

  if (response.success) {
    if (checkId) {
      dispatch(getUserCheckSuccess({
        response: normalize(response.idCheck, schema.idCheck),
      }));
    } else {
      dispatch(getUserCheckSuccess({
        response: normalize(response.idChecks, schema.arrayOfIdChecks),
      }));
    }
  } else {
    dispatch(getUserCheckFailure({
      message: response.error ?
        response.error.message || 'Something went wrong.' :
        'Something went wrong.',
      status: response.error ?
        response.error.status || 500 :
        500,
    }));
  }

  return response.success;
};

export const createUserCheck = id => async (dispatch, getState) => {
  if (getIsChecksActionLoading(getState(), 'create')) {
    return Promise.resolve();
  }

  dispatch(createUserCheckAttempt());

  const response = await api.createUserCheck(id);

  if (response.success) {
    dispatch(createUserCheckSuccess({
      response: normalize(response.idCheck, schema.idCheck),
    }));
  } else {
    dispatch(createUserCheckFailure({
      message: response.error ?
        response.error.message || 'Something went wrong.' :
        'Something went wrong.',
      status: response.error ?
        response.error.status || 500 :
        500,
    }));
  }

  return response.success;
};

export const uploadUserCheckFiles = (data, userId, checkId) => async (dispatch, getState) => {
  if (getIsChecksActionLoading(getState(), 'upload')) {
    return Promise.resolve();
  }

  dispatch(uploadUserCheckFilesAttempt());

  const response = await api.uploadUserCheckFiles(userId, checkId, data);

  if (response.success) {
    dispatch(uploadUserCheckFilesSuccess({
      response: normalize(response.idCheck, schema.idCheck),
    }));
  } else {
    dispatch(uploadUserCheckFilesFailure({
      message: response.error ?
        response.error.message || 'Something went wrong.' :
        'Something went wrong.',
      status: response.error ?
        response.error.status || 500 :
        500,
    }));
  }

  return response.success;
};
