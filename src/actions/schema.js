import { schema } from 'normalizr';
import { pick } from 'lodash';
import { userFields } from '../constants';

const user = new schema.Entity(
  'users',
  {},
  {
    idAttribute: '_id',
    processStrategy: value => pick(value, userFields),
  },
);

const idCheck = new schema.Entity(
  'idCheck',
  {},
  {
    idAttribute: '_id',
  },
);

const arrayOfIdChecks = new schema.Array(idCheck);

export { user, idCheck, arrayOfIdChecks };
