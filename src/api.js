import axios from 'axios';

const type = process.env.REACT_APP_CLIENT_TYPE ? process.env.REACT_APP_CLIENT_TYPE : 'consumer';

const baseURL = process.env.NODE_ENV === 'production' ? process.env.REACT_APP_API_URL : process.env.REACT_APP_DEV_API_URL;

const api = axios.create({
  baseURL,
  timeout: 60000,
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json',
  },
});

export const setApiAuthHeader = (token) => {
  api.defaults.headers.common.Authorization = `Bearer ${token}`;
};

export const removeApiAuthHeader = () => {
  delete api.defaults.headers.common.Authorization;
};

const handleError = (err) => {
  if (err.response) {
    return err.response.data;
  }
  return { success: false, error: { message: err.message } };
};

export const login = async (credentials) => {
  try {
    const response = await api.post('/auth/login', {
      ...credentials,
      type,
    });
    return response.data;
  } catch (err) {
    return handleError(err);
  }
};

export const register = async (data) => {
  try {
    const response = await api.post('/auth/register', {
      ...data,
      type,
    });
    return response.data;
  } catch (err) {
    return handleError(err);
  }
};

export const getUser = async (id) => {
  try {
    const response = await api.get(`/users/${id}`);
    return response.data;
  } catch (err) {
    return handleError(err);
  }
};

export const logout = async () => {
  try {
    const response = await api.post('/auth/logout');
    return response.data;
  } catch (err) {
    return handleError(err);
  }
};

export const validateUser = async (data) => {
  try {
    const response = await api.post('/auth/email', {
      ...data,
      type,
    });
    return response.data;
  } catch (err) {
    return handleError(err);
  }
};

export const getUserCheck = async (userId, checkId = null) => {
  try {
    const response = await api.get(`/users/${userId}/identitycheck/${checkId || ''}`);
    return response.data;
  } catch (err) {
    return handleError(err);
  }
};

export const createUserCheck = async (id) => {
  try {
    const response = await api.post(`/users/${id}/identitycheck`);
    return response.data;
  } catch (err) {
    return handleError(err);
  }
};

export const uploadUserCheckFiles = async (userId, checkId, data) => {
  const formData = new FormData();
  Object.entries(data).map(([key, files]) =>
    files.forEach(file =>
      formData.append(key, file)));
  try {
    const response = await api.post(`/users/${userId}/identitycheck/${checkId}/images`, formData);
    return response.data;
  } catch (err) {
    return handleError(err);
  }
};
