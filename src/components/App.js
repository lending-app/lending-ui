import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import '../css/App.css';

import ErrorBox from '../containers/ErrorBox';
import Header from '../containers/Header';
import Home from '../components/Home';
import Login from '../containers/Login';
import Register from '../containers/Register';
import Profile from '../containers/Profile';
import IdCheckList from '../containers/IdCheckList';
import IdCheckForm from '../containers/IdCheckForm';
import NotFound from '../components/NotFound';

const App = () => (
  <Router>
    <div>
      <Header />
      <div className="uk-section uk-section-default">
        <div className="uk-container uk-container-medium">
          <ErrorBox />
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/register" component={Register} />
            <Route exact path="/account/checks" component={IdCheckList} />
            <Route exact path="/account/profile" component={Profile} />
            <Route exact path="/account/checks/:checkId" component={IdCheckForm} />
            <Route exact path="/404" component={NotFound} />
            <Route component={NotFound} />
          </Switch>
        </div>
      </div>
    </div>
  </Router>
);

export default App;
