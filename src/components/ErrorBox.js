import React from 'react';

const ErrorBox = (props) => {
  const { closeErrorFunction } = props;
  const { error, isError } = props;

  if (!isError) {
    return null;
  }

  return (
    <div className="uk-alert uk-alert-danger" data-uk-alert>
      <button className="uk-alert-close" type="button" data-uk-close onClick={closeErrorFunction} />
      <h3>
        <strong>Error:</strong>
      </h3>
      <p>
        {error && error.message ? error.message : 'An undefined error occured'}
      </p>
    </div>
  );
};

export default ErrorBox;
