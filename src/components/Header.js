import React from 'react';
import { Link } from 'react-router-dom';
import { loggedOutRoutes, loggedInRoutes } from '../constants';

const renderLoginNav = location => loggedOutRoutes.map(route => (
  <li key={route.pathname} className={location === route.pathname ? 'uk-active' : ''}>
    <Link to={route.pathname}>{route.title}</Link>
  </li>
));

const renderLogin = location => (
  <ul className="uk-navbar-nav uk-visible@s">
    {renderLoginNav(location)}
  </ul>
);

class Header extends React.Component {
  constructor(props) {
    super(props);

    this.logOutClick = this.logOutClick.bind(this);
    this.renderNav = this.renderNav.bind(this);
    this.renderGreeting = this.renderGreeting.bind(this);
  }

  logOutClick(e) {
    e.preventDefault();
    const { logUserOutFunction } = this.props;
    logUserOutFunction();
  }

  renderNav(id, location) {
    const returnValue = loggedInRoutes.map(route => (
      <li key={route.pathname} className={location === route.pathname ? 'uk-active' : ''}>
        {
          location === route.pathname ?
          route.title :
          <Link to={route.pathname}>{route.title}</Link>
        }
      </li>
    ));

    returnValue.push([
      <li key="divider" className="uk-nav-divider" />,
      <li key="/logout">
        <a href="/logout" onClick={this.logOutClick}>Log Out</a>
      </li>,
    ]);

    return returnValue;
  }

  renderGreeting(name, id, location) {
    return (
      <ul className="uk-navbar-nav uk-visible@s">
        <li>
          <a>Welcome, {name}</a>
          <div className="uk-navbar-dropdown">
            <ul className="uk-nav uk-navbar-dropdown-nav">
              {this.renderNav(id, location)}
            </ul>
          </div>
        </li>
      </ul>
    );
  }

  render() {
    const { isAuthenticated, user: { firstName, lastName, _id: id }, location } = this.props;

    return (
      <div className="uk-offcanvas-content">
        <div id="offcanvas" uk-offcanvas="mode: push; overlay: true">
          <div className="uk-offcanvas-bar">
            <button className="uk-offcanvas-close" type="button" data-uk-close />
            <ul className="uk-nav uk-nav-primary uk-nav-center uk-margin-auto-vertical">
              { isAuthenticated ? this.renderNav(id, location) : renderLoginNav(location) }
            </ul>
          </div>
        </div>

        <div uk-sticky="sel-target: .uk-navbar-container; cls-active: uk-navbar-sticky; bottom: #transparent-sticky-navbar">
          <nav className="uk-navbar-container uk-margin" uk-navbar="mode: click" id="navMenu">

            <div className="uk-navbar-left">
              <Link className="uk-navbar-item uk-logo" to="/">Lending</Link>
            </div>

            <div className="uk-navbar-right">
              { isAuthenticated ? this.renderGreeting(`${firstName} ${lastName}`, id, location) : renderLogin(location) }

              <button className="uk-navbar-toggle uk-icon-link uk-hidden@s" uk-icon="menu" uk-toggle="target: #offcanvas" />
            </div>
          </nav>
        </div>
      </div>
    );
  }
}

export default Header;
