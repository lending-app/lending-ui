import React from 'react';
import { Link } from 'react-router-dom';

const IdCheck = (props) => {
  const { idCheck: { _id: id, ...idCheck } } = props;

  return (
    <li><Link to={`/account/checks/${id}`}>{id}</Link> - {JSON.stringify(idCheck, null, 2)}</li>
  );
};

export default IdCheck;
