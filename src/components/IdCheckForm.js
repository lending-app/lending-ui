import React from 'react';
import ImageUploader from 'react-images-upload';

import { withValidation } from '../hoc';
import {
  idDocumentFields,
  maxCountFrontFile,
  maxCountBackFile,
  maxCountFaceFile,
} from '../constants';
import validators from '../constants/validators';

const maxFilesCount = {
  front: maxCountFrontFile,
  back: maxCountBackFile,
  face: maxCountFaceFile,
};

const initialErrorsState = idDocumentFields.reduce(
  (prev, next) => ({ ...prev, [next.name]: { valid: true, msg: null } }),
  {},
);

const initialFieldsState = idDocumentFields.reduce(
  (prev, next) => ({ ...prev, [next.name]: next.initialValue }),
  {},
);

class IdCheckForm extends React.Component {
  constructor(props) {
    super(props);

    this.handleFileChange = this.handleFileChange.bind(this);
    this.handleKeyPress = this.handleKeyPress.bind(this);
    this.validateInput = this.validateInput.bind(this);
    this.validateSubmit = this.validateSubmit.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.createInputFields = this.createInputFields.bind(this);

    this.state = {
      errors: initialErrorsState,
      data: initialFieldsState,
      isValid: true,
    };
  }

  validateInput(field) {
    this.setState(prevState => ({
      errors: {
        ...prevState.errors,
        [field]: validators[field](prevState.data[field]),
      },
    }));
  }

  handleFileChange(name, file) {
    this.setState(
      prevState => ({
        data: {
          ...prevState.data,
          [name]: file,
        },
      }),
      () => this.validateInput(name),
    );
  }

  handleKeyPress(target) {
    if (target.charCode === 13) {
      target.preventDefault();
      this.handleSubmit();
    }
  }

  validateSubmit(cb) {
    idDocumentFields.map(field => this.validateInput(field.name));

    this.setState(
      prevState => ({
        isValid: !Object.values(prevState.errors).some(error => error.valid !== true),
      }),
      cb,
    );
  }

  handleSubmit(e = null) {
    if (e) {
      e.preventDefault();
    }

    this.validateSubmit(() => {
      const {
        idChecks,
        uploadUserCheckFilesFuntion,
        user,
        checkId,
      } = this.props;
      const { data, isValid } = this.state;

      if (isValid) {
        uploadUserCheckFilesFuntion(data, user._id, idChecks[checkId]._id);
      }
    });
  }

  createInputFields() {
    return idDocumentFields
      .map(({
        name,
        label,
        render,
        multiple,
      }) => {
        const { errors: { [name]: hasError } } = this.state;

        if (!render) {
          return null;
        }

        return (
          <div className="uk-width-1-2@s uk-text-center" key={name}>
            <label className="uk-form-label" htmlFor={name}>{label}</label>
            <div className="uk-form-controls">
              <InputWithValidation
                name={name}
                onChange={(file, pic) => this.handleFileChange(name, file, pic)}
                withPreview
                accept="image/jpeg"
                label={`Max files: ${maxFilesCount[name]}, Max file size: 5mb, accepted: jpg`}
                imgExtension={['.jpg']}
                maxFileSize={5 * 1024 * 1024}
                singleImage={!multiple}
                hasError={hasError.valid !== null && !hasError.valid}
                errorMsg={hasError.msg}
              />
            </div>
          </div>
        );
      });
  }

  render() {
    return (
      <form
        onSubmit={this.handleSubmit}
        className="uk-flex-center uk-form-stacked uk-grid-divider"
        data-uk-grid
      >
        {this.createInputFields()}

        <div className="uk-width-1-1 uk-form-controls uk-text-center">
          <button className="uk-button uk-button-primary">Upload</button>
        </div>
      </form>
    );
  }
}

const InputWithValidation = withValidation(props => <ImageUploader {...props} />);

export default IdCheckForm;
