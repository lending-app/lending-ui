import React from 'react';

import IdCheck from './IdCheck';

class IdCheckList extends React.Component {
  constructor(props) {
    super(props);

    this.handleButtonClick = this.handleButtonClick.bind(this);
  }

  handleButtonClick(e) {
    e.preventDefault();
    const { user, createUserCheckFuntion } = this.props;
    createUserCheckFuntion(user._id);
  }

  render() {
    const { idChecks } = this.props;

    return (
      <div>
        <h2>User checks</h2>
        <ul className="uk-list uk-list-large uk-list-striped">
          {
            Object.entries(idChecks).map(([key, idCheck]) => (
              <IdCheck
                key={key}
                idCheck={idCheck}
              />
            ))
          }
        </ul>
        <button className="uk-button uk-button-primary uk-align-center" onClick={this.handleButtonClick}>Create new ID Check</button>
      </div>
    );
  }
}

export default IdCheckList;
