import React from 'react';
import { ClipLoader } from 'react-spinners';

const Loading = () => (
  <div>
    <ClipLoader
      loaderStyle={{
        display: 'block',
        margin: '0 auto',
      }}
      loading
    />
  </div>
);

export default Loading;
