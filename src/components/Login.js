import React from 'react';

import { withValidation } from '../hoc';
import { loginFields } from '../constants';
import validators from '../constants/validators';

const initialErrorsState = loginFields.reduce(
  (prev, next) => ({ ...prev, [next.name]: { valid: null, msg: null } }),
  {},
);

const initialFieldsState = loginFields.reduce(
  (prev, next) => ({ ...prev, [next.name]: next.initialValue }),
  {},
);

class LoginPage extends React.Component {
  constructor(props) {
    super(props);

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleKeyPress = this.handleKeyPress.bind(this);
    this.validateInput = this.validateInput.bind(this);
    this.validateSubmit = this.validateSubmit.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.createInputFields = this.createInputFields.bind(this);

    this.state = {
      data: initialFieldsState,
      errors: initialErrorsState,
      isValid: false,
    };
  }

  validateInput(field) {
    this.setState(prevState => ({
      errors: {
        ...prevState.errors,
        [field]: validators[field](prevState.data[field], true),
      },
    }));
  }

  handleInputChange(e) {
    const { id: target } = e.currentTarget;
    const { value } = e.target;

    this.setState(
      prevState => ({
        data: {
          ...prevState.data,
          [target]: value,
        },
      }),
      () => this.validateInput(target),
    );
  }

  handleKeyPress(target) {
    if (target.charCode === 13) {
      target.preventDefault();
      this.handleSubmit();
    }
  }

  validateSubmit(cb) {
    loginFields.map(field => this.validateInput(field.name));

    this.setState(
      prevState => ({
        isValid: !Object.values(prevState.errors).some(error => error.valid !== true),
      }),
      cb,
    );
  }

  handleSubmit(e = null) {
    if (e) {
      e.preventDefault();
    }

    this.validateSubmit(() => {
      const { loginFunction } = this.props;
      const { data, isValid } = this.state;
      if (isValid) {
        loginFunction(data);
      }
    });
  }

  createInputFields() {
    return loginFields
      .map(({
        name,
        type,
        placeholder,
        label,
        render,
        props = {},
      }) => {
        if (!render) {
          return null;
        }

        const { [name]: hasError } = this.state.errors;

        return (
          <div className="uk-width-1-1" key={name}>
            <label className="uk-form-label" htmlFor={name}>{label}</label>
            <div className="uk-form-controls">
              <InputWithValidation
                className="uk-input"
                id={name}
                name={name}
                type={type}
                placeholder={placeholder}
                value={this.state.data[name]}
                onChange={this.handleInputChange}
                onKeyPress={this.handleKeyPress}
                hasError={hasError.valid !== null && !hasError.valid}
                errorMsg={hasError.msg}
                {...props}
              />
            </div>
          </div>
        );
      });
  }

  render() {
    return (
      <form
        onSubmit={this.handleSubmit}
        className="uk-flex-center uk-form-stacked"
        data-uk-grid
      >
        {this.createInputFields()}

        <div className="uk-width-1-1 uk-form-controls uk-text-center">
          <button className="uk-button uk-button-primary">Log In</button>
        </div>

      </form>
    );
  }
}

const InputWithValidation = withValidation(inputProps => <input {...inputProps} />);

export default LoginPage;
