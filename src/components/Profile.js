import React from 'react';

const Profile = (props) => {
  const { user } = props;

  return (
    <div>
      <h2>Profile</h2>
      <div>{JSON.stringify(user, null, 2)}</div>
    </div>
  );
};

export default Profile;
