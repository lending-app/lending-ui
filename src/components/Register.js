import React from 'react';
import PhoneInput from 'react-phone-number-input';
import 'react-phone-number-input/style.css';

import { withValidation } from '../hoc';
import { registerFields } from '../constants';
import validators from '../constants/validators';
import errorMessages from '../constants/errors';

const initialErrorsState = registerFields.reduce(
  (prev, next) => ({ ...prev, [next.name]: { valid: true, msg: null } }),
  {},
);

const initialFieldsState = registerFields.reduce(
  (prev, next) => ({ ...prev, [next.name]: next.initialValue }),
  {},
);

class RegisterPage extends React.Component {
  constructor(props) {
    super(props);

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleEmailBlur = this.handleEmailBlur.bind(this);
    this.handleKeyPress = this.handleKeyPress.bind(this);
    this.validateInput = this.validateInput.bind(this);
    this.validateSubmit = this.validateSubmit.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.createInputFields = this.createInputFields.bind(this);

    this.state = {
      data: initialFieldsState,
      errors: initialErrorsState,
      isValid: true,
      userExists: false,
    };
  }

  validateInput(field) {
    this.setState(prevState => ({
      errors: {
        ...prevState.errors,
        [field]: validators[field](prevState.data[field]),
      },
    }));
  }

  handleInputChange(e) {
    const { id: target } = e.currentTarget;
    const { value } = e.target;

    this.setState(
      prevState => ({
        data: {
          ...prevState.data,
          [target]: value,
        },
      }),
      () => this.validateInput(target),
    );
  }

  handleKeyPress(target) {
    if (target.charCode === 13) {
      target.preventDefault();
      this.handleSubmit();
    }
  }

  async handleEmailBlur() {
    const { validateUserFunction } = this.props;
    const { data: { email: data }, errors: { email: error } } = this.state;
    if (error.valid) {
      const result = await validateUserFunction({ email: data });
      this.setState({
        userExists: result.success && !result.result,
      });
    }
  }

  validateSubmit(cb) {
    const { userExists } = this.state;
    const fields = userExists ? registerFields.filter(field => field.name === 'email' || field.name === 'password') : registerFields;

    fields.map(field => this.validateInput(field.name));

    this.setState(
      (prevState) => {
        let errors = Object.entries(prevState.errors);

        if (userExists) {
          errors = errors.filter(([field]) => ['email', 'password'].includes(field));
        }

        return {
          isValid: !errors.some(([, error]) => error.valid !== true),
        };
      },
      cb,
    );
  }

  handleSubmit(e = null) {
    if (e) {
      e.preventDefault();
    }

    this.validateSubmit(() => {
      const { registerFunction } = this.props;
      const { data: formData, isValid, userExists } = this.state;

      let data = {};
      if (userExists) {
        data = {
          email: formData.email,
          password: formData.password,
        };
      } else {
        data = formData;
      }

      if (isValid) {
        registerFunction(data);
      }
    });
  }

  createInputFields() {
    return registerFields
      .map(({
        name,
        type,
        placeholder,
        label,
        render,
        props = {},
      }) => {
        const { errors: { [name]: hasError }, userExists } = this.state;

        if (!render || (userExists && !['email', 'password'].includes(name))) {
          return null;
        }

        return (
          <div className="uk-width-1-2@s" key={name}>
            <label className="uk-form-label" htmlFor={name}>{label}</label>
            <div className="uk-form-controls">
              <InputWithValidation
                className="uk-input"
                id={name}
                name={name}
                type={type}
                placeholder={placeholder}
                value={this.state.data[name]}
                onChange={this.handleInputChange}
                onBlur={e => (name === 'email' ? this.handleEmailBlur(e) : undefined)}
                onKeyPress={this.handleKeyPress}
                hasError={hasError.valid !== null && !hasError.valid}
                errorMsg={hasError.msg}
                {...props}
              />
              {
                name === 'email' && userExists ?
                  <span className="uk-text-info">{errorMessages.email.unique}</span> :
                  null
              }
            </div>
          </div>
        );
      });
  }

  render() {
    return (
      <form
        onSubmit={this.handleSubmit}
        className="uk-flex-center uk-form-stacked"
        data-uk-grid
      >
        {this.createInputFields()}

        <div className="uk-width-1-1 uk-form-controls uk-text-center">
          <button className="uk-button uk-button-primary">Register</button>
        </div>

      </form>
    );
  }
}

const InputWithValidation = withValidation((inputProps) => {
  if (inputProps.name === 'phoneNumber') {
    const { onChange, className, ...restProps } = inputProps;

    return (
      <PhoneInput
        {...restProps}
        onChange={phoneNumber => onChange({
          currentTarget: {
            id: 'phoneNumber',
          },
          target: {
            value: phoneNumber,
          },
        })}
      />
    );
  }

  return <input {...inputProps} />;
});

export default RegisterPage;
