import { maxCountFrontFile, maxCountBackFile, maxCountFaceFile } from './index';

export default {
  email: {
    required: 'Email is required',
    email: 'Enter a valid email address',
    unique: 'It seem that you already have an account. Please Log-In',
  },
  password: {
    required: 'Password is required',
    length: 'Password must be at least 8 characters long',
    pattern: 'Enter a valid password: Must have at least an upper case letter and a number',
    confirmation: 'Passwords don\'t match',
  },
  firstName: {
    required: 'First name is required',
    pattern: 'Enter a valid first name',
  },
  lastName: {
    required: 'Last name is required',
    pattern: 'Enter a valid last name',
  },
  phoneNumber: {
    required: 'Phone number is required',
    valid: 'Enter a valid phone number',
  },
  birthdate: {
    required: 'Birthdate is required',
    valid: 'Enter a valid birthdate',
    older: 'You must be at least 18 years old',
  },
  front: {
    required: 'ID card front side is required',
    max: `Can only upload ${maxCountFrontFile} file(s) for this field`,
  },
  back: {
    required: 'ID card back side is required',
    max: `Can only upload ${maxCountBackFile} file(s) for this field`,
  },
  face: {
    required: 'At least one photo of your face is required',
    max: `Can only upload ${maxCountFaceFile} file(s) for this field`,
  },
};
