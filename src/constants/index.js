import moment from 'moment';

export const userFields = [
  '_id',
  'email',
  'firstName',
  'lastName',
  'createdAt',
  'birthdate',
  'phoneNumber',
  'roles',
];

const maxDate = moment().subtract(18, 'y').format('YYYY-MM-DD');

export const loginFields = [
  {
    name: 'email',
    initialValue: '',
    type: 'email',
    label: 'Email',
    placeholder: 'contact@mail.com',
    render: true,
  },
  {
    name: 'password',
    initialValue: '',
    type: 'password',
    label: 'Password',
    placeholder: 'password',
    render: true,
  },
];

export const idDocumentFields = [
  {
    name: 'front',
    initialValue: [],
    type: 'file',
    label: 'Front',
    multiple: false,
    render: true,
  },
  {
    name: 'back',
    initialValue: [],
    type: 'file',
    label: 'Back',
    multiple: false,
    render: true,
  },
  {
    name: 'face',
    initialValue: [],
    type: 'file',
    label: 'Face',
    multiple: true,
    render: true,
  },
];

export const registerFields = [
  ...loginFields,
  {
    name: 'firstName',
    initialValue: '',
    type: 'text',
    label: 'First Name',
    placeholder: 'John',
    render: true,
  },
  {
    name: 'lastName',
    initialValue: '',
    type: 'text',
    label: 'Last Name',
    placeholder: 'Smith',
    render: true,
  },
  {
    name: 'phoneNumber',
    initialValue: '',
    type: 'tel',
    label: 'Phone Number',
    placeholder: 'Enter a phone number',
    render: true,
    props: {
      country: 'CO',
      countries: ['CO'],
      inputClassName: 'uk-input',
    },
  },
  {
    name: 'birthdate',
    initialValue: '',
    type: 'date',
    label: 'Birthdate',
    placeholder: 'Enteryour birth date',
    render: true,
    props: {
      max: maxDate,
    },
  },
];

export const passwordPattern = '^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$';
export const namePattern = '^[a-zA-ZÀ-ž][\\sa-zA-ZÀ-ž]*$';
export const maxCountFrontFile = 1;
export const maxCountBackFile = 1;
export const maxCountFaceFile = 2;

export const loggedOutRoutes = [
  { pathname: '/login', title: 'Log In' },
  { pathname: '/register', title: 'Register' },
];

export const loggedInRoutes = [
  { pathname: '/account/profile', title: 'Profile' },
  { pathname: '/account/checks', title: 'ID Checks' },
];

export const loadingCondition = props => props.isLoading;

export const noAuthCondition = props => !props.isAuthenticated;

export const authCondition = (props) => {
  if (loadingCondition(props)) {
    return true;
  }

  return props.isAuthenticated;
};
