import validator from 'validator';
import moment from 'moment';
import { isValidNumber } from 'libphonenumber-js';
import errorMessages from './errors';

import {
  passwordPattern,
  namePattern,
  maxCountFrontFile,
  maxCountBackFile,
  maxCountFaceFile,
} from './index';

const emailValidator = (value) => {
  if (validator.isEmpty(value)) {
    return {
      valid: false,
      msg: errorMessages.email.required,
    };
  }

  if (!validator.isEmail(value)) {
    return {
      valid: false,
      msg: errorMessages.email.email,
    };
  }

  return {
    valid: true,
    msg: null,
  };
};

const passwordValidator = (value, login = false) => {
  if (validator.isEmpty(value)) {
    return {
      valid: false,
      msg: errorMessages.password.required,
    };
  }

  if (!login && !validator.isLength(value, { min: 8 })) {
    return {
      valid: false,
      msg: errorMessages.password.length,
    };
  }

  if (!login && !validator.matches(value, passwordPattern, 'g')) {
    return {
      valid: false,
      msg: errorMessages.password.pattern,
    };
  }

  return {
    valid: true,
    msg: null,
  };
};

const firstNameValidator = (value) => {
  if (validator.isEmpty(value)) {
    return {
      valid: false,
      msg: errorMessages.firstName.required,
    };
  }

  if (!validator.matches(value, namePattern, 'g')) {
    return {
      valid: false,
      msg: errorMessages.firstName.pattern,
    };
  }

  return {
    valid: true,
    msg: null,
  };
};

const lastNameValidator = (value) => {
  if (validator.isEmpty(value)) {
    return {
      valid: false,
      msg: errorMessages.lastName.required,
    };
  }

  if (!validator.matches(value, namePattern, 'g')) {
    return {
      valid: false,
      msg: errorMessages.lastName.pattern,
    };
  }

  return {
    valid: true,
    msg: null,
  };
};

const phoneNumberValidator = (value = '') => {
  if (validator.isEmpty(value)) {
    return {
      valid: false,
      msg: errorMessages.phoneNumber.required,
    };
  }

  if (!isValidNumber(value, 'CO')) {
    return {
      valid: false,
      msg: errorMessages.phoneNumber.valid,
    };
  }

  return {
    valid: true,
    msg: null,
  };
};

const birthdateValidator = (value) => {
  if (validator.isEmpty(value)) {
    return {
      valid: false,
      msg: errorMessages.birthdate.required,
    };
  }

  const birthdate = moment(value);

  if (!validator.isBefore(value, moment().toString())) {
    return {
      valid: false,
      msg: errorMessages.birthdate.valid,
    };
  }

  if (moment().diff(birthdate, 'years') < 18) {
    return {
      valid: false,
      msg: errorMessages.birthdate.older,
    };
  }

  return {
    valid: true,
    msg: null,
  };
};

const frontValidator = (value) => {
  if (value.length === 0) {
    return {
      valid: false,
      msg: errorMessages.front.required,
    };
  }

  if (value.length > maxCountFrontFile) {
    return {
      valid: false,
      msg: errorMessages.front.max,
    };
  }

  return {
    valid: true,
    msg: null,
  };
};

const backValidator = (value) => {
  if (value.length === 0) {
    return {
      valid: false,
      msg: errorMessages.back.required,
    };
  }

  if (value.length > maxCountBackFile) {
    return {
      valid: false,
      msg: errorMessages.back.max,
    };
  }

  return {
    valid: true,
    msg: null,
  };
};

const faceValidator = (value) => {
  if (value.length === 0) {
    return {
      valid: false,
      msg: errorMessages.face.required,
    };
  }

  if (value.length > maxCountFaceFile) {
    return {
      valid: false,
      msg: errorMessages.face.max,
    };
  }

  return {
    valid: true,
    msg: null,
  };
};

export default {
  email: emailValidator,
  password: passwordValidator,
  firstName: firstNameValidator,
  lastName: lastNameValidator,
  phoneNumber: phoneNumberValidator,
  birthdate: birthdateValidator,
  front: frontValidator,
  back: backValidator,
  face: faceValidator,
};
