import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, compose } from 'redux';
import { withRouter } from 'react-router-dom';
import { getIsError, getError } from '../reducers';
import { clearError } from '../actions/error';

import ErrorBox from '../components/ErrorBox';

const redirectIfErrors = (props) => {
  const { error, history } = props;

  if (error.status === 404) {
    history.replace('/404');
  } else if (error.status === 403) {
    // TODO: Redirect to previous page
    history.replace('/account/checks');
  }
};

class ErrorBoxContainer extends React.Component {
  componentWillMount() {
    redirectIfErrors(this.props);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.error.message !== this.props.error.message) {
      redirectIfErrors(nextProps);
    }
  }

  render() {
    const {
      isError,
      error,
      closeErrorFunction,
    } = this.props;

    return (
      <ErrorBox
        isError={isError}
        error={error}
        closeErrorFunction={closeErrorFunction}
      />
    );
  }
}

const mapStateToProps = state => ({
  isError: getIsError(state),
  error: getError(state),
});

const mapDispatchToProps = dispatch => bindActionCreators({
  closeErrorFunction: clearError,
  dispatch,
}, dispatch);

export default compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps),
)(ErrorBoxContainer);
