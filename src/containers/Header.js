import React from 'react';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import { getIsAuthenticated, getUserInfo } from '../reducers';
import { logout } from '../actions/auth';

import Header from '../components/Header';

const HeaderContainer = (props) => {
  const {
    isAuthenticated,
    user,
    location: { pathname },
    logUserOutFunction,
  } = props;

  return (
    <Header
      isAuthenticated={isAuthenticated}
      user={user}
      logUserOutFunction={logUserOutFunction}
      location={pathname}
    />
  );
};

const mapStateToProps = state => ({
  isAuthenticated: getIsAuthenticated(state),
  user: getUserInfo(state),
});

const mapDispatchToProps = dispatch => bindActionCreators({
  logUserOutFunction: logout,
  dispatch,
}, dispatch);

export default compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps),
)(HeaderContainer);
