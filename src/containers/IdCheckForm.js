import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { compose, bindActionCreators } from 'redux';
import {
  getIdChecks,
  getIsAuthenticated,
  getIsAuthActionLoading,
  getIsChecksActionLoading,
  getIsCheckActionSuccess,
  getUserInfo,
} from '../reducers';
import { clearUserChecks, getUserCheck, uploadUserCheckFiles } from '../actions/idChecks';
import * as enhance from '../hoc';
import { authCondition, loadingCondition } from '../constants';

import IdCheckForm from '../components/IdCheckForm';

class IdCheckFormContainer extends React.Component {
  constructor(props) {
    super(props);

    this.fetchIdCheck = this.fetchIdCheck.bind(this);
  }

  componentDidMount() {
    this.fetchIdCheck();
  }

  componentDidUpdate(prevProps) {
    const { isAuthenticated } = this.props;
    if (prevProps.isAuthenticated !== isAuthenticated) {
      this.fetchIdCheck();
    }
  }

  componentWillUnmount() {
    const { clearUserChecksFunction } = this.props;
    clearUserChecksFunction();
  }

  fetchIdCheck() {
    const {
      getUserCheckFuntion,
      isAuthenticated,
      user,
      match: { params },
    } = this.props;

    if (isAuthenticated) {
      getUserCheckFuntion(user._id, params.checkId);
    }
  }

  render() {
    const {
      idChecks,
      isUploadSuccess,
      match: { params },
      uploadUserCheckFilesFuntion,
      user,
    } = this.props;

    if (isUploadSuccess) {
      return (
        <Redirect
          to={{
            pathname: '/account/checks',
            state: {
              idCheck: params.checkId,
              success: isUploadSuccess,
            },
          }}
        />
      );
    }

    return (
      <IdCheckForm
        idChecks={idChecks}
        uploadUserCheckFilesFuntion={uploadUserCheckFilesFuntion}
        user={user}
        checkId={params.checkId}
      />
    );
  }
}

const mapStateToProps = state => ({
  isAuthenticated: getIsAuthenticated(state),
  isLoading: getIsAuthActionLoading(state, ['getUser', 'login']) || getIsChecksActionLoading(state, ['get', 'upload']),
  isUploadSuccess: getIsCheckActionSuccess(state, 'upload'),
  user: getUserInfo(state),
  idChecks: getIdChecks(state),
});

const mapDispatchToProps = dispatch => bindActionCreators({
  clearUserChecksFunction: clearUserChecks,
  getUserCheckFuntion: getUserCheck,
  uploadUserCheckFilesFuntion: uploadUserCheckFiles,
  dispatch,
}, dispatch);

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  enhance.withProtectedRoute(authCondition),
  enhance.withLoading(loadingCondition),
)(IdCheckFormContainer);
