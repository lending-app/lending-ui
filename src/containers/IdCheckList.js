import React from 'react';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import {
  getIdChecks,
  getIsAuthenticated,
  getIsAuthActionLoading,
  getIsChecksActionLoading,
  getUserInfo,
} from '../reducers';
import { clearUserChecks, createUserCheck, getUserCheck } from '../actions/idChecks';
import * as enhance from '../hoc';
import { authCondition, loadingCondition } from '../constants';

import IdCheckList from '../components/IdCheckList';

class IdCheckListContainer extends React.Component {
  componentDidMount() {
    this.fetchIdChecks();
  }

  componentDidUpdate(prevProps) {
    const { isAuthenticated } = this.props;
    if (prevProps.isAuthenticated !== isAuthenticated) {
      this.fetchIdChecks();
    }
  }

  componentWillUnmount() {
    const { clearUserChecksFunction } = this.props;
    clearUserChecksFunction();
  }

  fetchIdChecks() {
    const { getUserCheckFuntion, isAuthenticated, user } = this.props;

    if (isAuthenticated) {
      getUserCheckFuntion(user._id);
    }
  }

  render() {
    const {
      createUserCheckFuntion,
      idChecks,
      location,
      user,
    } = this.props;
    const { idCheck, success } = location.state || { idCheck: null, success: false };

    return (
      <div>
        {
          idCheck && success ?
            (
              <div className="uk-alert uk-alert-success" data-uk-alert>
                <button className="uk-alert-close" type="button" data-uk-close />
                <h3>
                  <strong>Success:</strong>
                </h3>
                <p>Images for Id Check #{idCheck} uploaded successfully</p>
              </div>
            ) : null
        }
        <IdCheckList
          user={user}
          idChecks={idChecks}
          createUserCheckFuntion={createUserCheckFuntion}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isAuthenticated: getIsAuthenticated(state),
  isLoading: getIsAuthActionLoading(state, ['getUser', 'login']) || getIsChecksActionLoading(state, ['get', 'create']),
  user: getUserInfo(state),
  idChecks: getIdChecks(state),
});

const mapDispatchToProps = dispatch => bindActionCreators({
  clearUserChecksFunction: clearUserChecks,
  getUserCheckFuntion: getUserCheck,
  createUserCheckFuntion: createUserCheck,
  dispatch,
}, dispatch);

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  enhance.withProtectedRoute(authCondition),
  enhance.withLoading(loadingCondition),
)(IdCheckListContainer);
