import React from 'react';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import { login } from '../actions/auth';
import { getIsAuthenticated, getIsAuthActionLoading } from '../reducers';
import * as enhance from '../hoc';
import { noAuthCondition, loadingCondition } from '../constants';

import Login from '../components/Login';

const LoginContainer = (props) => {
  const { loginFunction } = props;

  return (
    <Login loginFunction={loginFunction} />
  );
};

const mapStateToProps = state => ({
  isAuthenticated: getIsAuthenticated(state),
  isLoading: getIsAuthActionLoading(state, 'login'),
});

const mapDispatchToProps = dispatch => bindActionCreators({
  loginFunction: login,
  dispatch,
}, dispatch);

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  enhance.withNoAuthRoute(noAuthCondition),
  enhance.withLoading(loadingCondition),
)(LoginContainer);
