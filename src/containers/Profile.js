import React from 'react';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import { getIsAuthenticated, getIsAuthActionLoading, getUserInfo } from '../reducers';
import { getUser } from '../actions/auth';
import * as enhance from '../hoc';
import { authCondition, loadingCondition } from '../constants';

import Profile from '../components/Profile';

class ProfileContainer extends React.Component {
  componentDidMount() {
    const { getUserFuntion, user } = this.props;
    getUserFuntion(user._id);
  }

  render() {
    const { user } = this.props;

    return (
      <Profile user={user} />
    );
  }
}

const mapStateToProps = state => ({
  isAuthenticated: getIsAuthenticated(state),
  isLoading: getIsAuthActionLoading(state, ['getUser', 'login']),
  user: getUserInfo(state),
});

const mapDispatchToProps = dispatch => bindActionCreators({
  getUserFuntion: getUser,
  dispatch,
}, dispatch);

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  enhance.withProtectedRoute(authCondition),
  enhance.withLoading(loadingCondition),
)(ProfileContainer);
