import React from 'react';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import { register, validateUser } from '../actions/auth';
import { getIsAuthenticated, getIsAuthActionLoading } from '../reducers';
import * as enhance from '../hoc';
import { noAuthCondition, loadingCondition } from '../constants';

import Register from '../components/Register';

const RegisterContainer = (props) => {
  const { registerFunction, validateUserFunction } = props;
  return (
    <Register
      registerFunction={registerFunction}
      validateUserFunction={validateUserFunction}
    />
  );
};

const mapStateToProps = state => ({
  isAuthenticated: getIsAuthenticated(state),
  isLoading: getIsAuthActionLoading(state, 'registration'),
});

const mapDispatchToProps = dispatch => bindActionCreators({
  registerFunction: register,
  validateUserFunction: validateUser,
  dispatch,
}, dispatch);

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  enhance.withNoAuthRoute(noAuthCondition),
  enhance.withLoading(loadingCondition),
)(RegisterContainer);
