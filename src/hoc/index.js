import React from 'react';
import { Redirect } from 'react-router-dom';
import Loading from '../components/Loading';

export const withLoading = conditionFn => Component => props => (
  <div>
    <Component {...props} />

    {conditionFn(props) && <Loading />}
  </div>
);

export const withProtectedRoute = conditionFn => Component => props => (
  conditionFn(props) ?
    <Component {...props} /> :
    <Redirect to="/login" />
);

export const withNoAuthRoute = conditionFn => Component => props => (
  conditionFn(props) ?
    <Component {...props} /> :
    <Redirect to="/" />
);

export const withValidation = Component => ({
  className,
  hasError,
  errorMsg,
  ...props
}) => (
  <div key={props.name}>
    <Component className={`${className} ${hasError ? 'uk-form-danger' : ''}`} {...props} />
    <span className={`uk-text-danger ${!hasError ? 'uk-invisible' : ''}`}>{errorMsg}</span>
  </div>
);
