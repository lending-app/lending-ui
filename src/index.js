import 'uikit/dist/css/uikit.css';
import React from 'react';
import ReactDOM from 'react-dom';
import UIkit from 'uikit';
import Icons from 'uikit/dist/js/uikit-icons';
import './index.css';
import App from './components/Root';
import registerServiceWorker from './registerServiceWorker';
import configureStore from './store';

const store = configureStore();

UIkit.use(Icons);

ReactDOM.render(
  <App store={store} />,
  document.getElementById('root'),
);

registerServiceWorker();
