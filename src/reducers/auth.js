import { combineReducers } from 'redux';

const createAuthState = () => {
  const isAuthenticated = (state = false, action) => {
    switch (action.type) {
      case 'AUTH_LOGIN_FAILURE':
      case 'AUTH_GETUSER_FAILURE':
      case 'AUTH_LOGOUT_SUCCESS':
        return false;
      case 'AUTH_LOGIN_SUCCESS':
      case 'AUTH_GETUSER_SUCCESS':
        return true;
      default:
        return state;
    }
  };

  const userInfo = (state = {}, action) => {
    switch (action.type) {
      case 'AUTH_LOGIN_SUCCESS':
      case 'AUTH_GETUSER_SUCCESS':
        return action.data.response.entities.users[action.data.response.result];
      case 'AUTH_REGISTRATION_FAILURE':
      case 'AUTH_LOGIN_FAILURE':
      case 'AUTH_GETUSER_FAILURE':
      case 'AUTH_LOGOUT_SUCCESS':
        return {};
      default:
        return state;
    }
  };

  const authToken = (state = null, action) => {
    switch (action.type) {
      case 'AUTH_LOGIN_SUCCESS':
      case 'AUTH_GETUSER_SUCCESS':
        if (typeof action.data.token === 'undefined') {
          return state;
        }
        return action.data.token;
      case 'AUTH_REGISTRATION_FAILURE':
      case 'AUTH_LOGIN_FAILURE':
      case 'AUTH_GETUSER_FAILURE':
      case 'AUTH_LOGOUT_SUCCESS':
        return null;
      default:
        return state;
    }
  };

  return combineReducers({
    isAuthenticated,
    userInfo,
    authToken,
  });
};

export default createAuthState;

export const getIsAuthenticated = state => state.isAuthenticated;
export const getUserInfo = state => state.userInfo;
export const getAuthToken = state => state.authToken;
