import { combineReducers } from 'redux';

const createAuthActionState = (actionName) => {
  const isLoading = (state = false, action) => {
    if (actionName !== action.actionName) {
      return state;
    }

    switch (action.type) {
      case `AUTH_${actionName.toUpperCase()}_ATTEMPT`:
        return true;
      case `AUTH_${actionName.toUpperCase()}_SUCCESS`:
      case `AUTH_${actionName.toUpperCase()}_FAILURE`:
        return false;
      default:
        return state;
    }
  };

  return combineReducers({
    isLoading,
  });
};

export default createAuthActionState;

export const getIsAuthActionLoading = state => state.isLoading;
