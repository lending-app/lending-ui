import { combineReducers } from 'redux';

const createErrorState = () => {
  const isError = (state = false, action) => {
    if (action.type.endsWith('FAILURE')) {
      return true;
    } else if (action.type === 'ERROR_CLEARED') {
      return false;
    }

    return state;
  };

  const error = (state = {}, action) => {
    if (action.type.endsWith('FAILURE')) {
      return action.error;
    } else if (action.type === 'ERROR_CLEARED') {
      return {};
    }

    return state;
  };

  return combineReducers({
    isError,
    error,
  });
};

export default createErrorState;

export const getIsError = state => state.isError;
export const getError = state => state.error;
