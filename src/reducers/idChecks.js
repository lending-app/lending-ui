import { combineReducers } from 'redux';

const createChecksState = () => {
  const ids = (state = [], action) => {
    switch (action.type) {
      case 'CHECKS_GET_SUCCESS':
        return action.data.response.result;
      case 'CHECKS_CREATE_SUCCESS':
        return [...state, action.data.response.result];
      case 'CHECKS_CLEAR':
        return [];
      default:
        return state;
    }
  };

  const byId = (state = {}, action) => {
    if (action.type.startsWith('CHECKS_')) {
      if (action.type === 'CHECKS_CLEAR') {
        return {};
      }

      if (action.data && action.data.response && action.data.response.entities.idCheck) {
        return {
          ...state,
          ...action.data.response.entities.idCheck,
        };
      }
    }

    return state;
  };

  return combineReducers({
    ids,
    byId,
  });
};

export default createChecksState;

export const getIds = state => state.ids;
export const getIdChecks = state => state.byId;
export const getIdCheck = (state, id) => state.byId[id];
