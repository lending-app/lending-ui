import { combineReducers } from 'redux';

const createChecksActionState = (actionName) => {
  const isLoading = (state = false, action) => {
    if (actionName !== action.actionName) {
      return state;
    }

    switch (action.type) {
      case `CHECKS_${actionName.toUpperCase()}_ATTEMPT`:
        return true;
      case `CHECKS_${actionName.toUpperCase()}_SUCCESS`:
      case `CHECKS_${actionName.toUpperCase()}_FAILURE`:
      case 'CHECKS_CLEAR':
        return false;
      default:
        return state;
    }
  };

  const success = (state = false, action) => {
    if (actionName !== action.actionName) {
      return state;
    }

    switch (action.type) {
      case `CHECKS_${actionName.toUpperCase()}_SUCCESS`:
        return true;
      case `CHECKS_${actionName.toUpperCase()}_ATTEMPT`:
      case `CHECKS_${actionName.toUpperCase()}_FAILURE`:
      case 'CHECKS_CLEAR':
        return false;
      default:
        return state;
    }
  };

  return combineReducers({
    isLoading,
    success,
  });
};

export default createChecksActionState;

export const getIsCheckActionLoading = state => state.isLoading;
export const getIsCheckActionSuccess = state => state.success;
