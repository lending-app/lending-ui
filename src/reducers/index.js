import { combineReducers } from 'redux';
import createAuthState, * as fromAuth from './auth';
import createChecksState, * as fromChecks from './idChecks';
import createErrorState, * as fromError from './error';
import createAuthActionState, * as fromAuthAction from './authActions';
import createChecksActionState, * as fromChecksAction from './idChecksActions';

const checksActionState = combineReducers({
  get: createChecksActionState('get'),
  create: createChecksActionState('create'),
  upload: createChecksActionState('upload'),
});

const authActionState = combineReducers({
  logout: createAuthActionState('logout'),
  login: createAuthActionState('login'),
  registration: createAuthActionState('registration'),
  getUser: createAuthActionState('getUser'),
  validateUser: createAuthActionState('validateUser'),
});

const actionState = combineReducers({
  checks: checksActionState,
  auth: authActionState,
});

const reducers = combineReducers({
  auth: createAuthState(),
  error: createErrorState(),
  checks: createChecksState(),
  actionState,
});

export default reducers;

export const getIsAuthenticated = state =>
  fromAuth.getIsAuthenticated(state.auth);

export const getIsAuthActionLoading = (state, actions) => {
  if (typeof actions === 'string') {
    return fromAuthAction.getIsAuthActionLoading(state.actionState.auth[actions]);
  }

  return actions
    .map(action => fromAuthAction.getIsAuthActionLoading(state.actionState.auth[action]))
    .find(error => error !== false);
};

export const getUserInfo = state =>
  fromAuth.getUserInfo(state.auth);

export const getAuthToken = state =>
  fromAuth.getAuthToken(state.auth);

export const getIsError = state =>
  fromError.getIsError(state.error);

export const getError = state =>
  fromError.getError(state.error);

export const getIsChecksActionLoading = (state, actions) => {
  if (typeof actions === 'string') {
    return fromChecksAction.getIsCheckActionLoading(state.actionState.checks[actions]);
  }

  return actions
    .map(action => fromChecksAction.getIsCheckActionLoading(state.actionState.checks[action]))
    .find(error => error !== false);
};

export const getIsCheckActionSuccess = (state, action) =>
  fromChecksAction.getIsCheckActionSuccess(state.actionState.checks[action]);

export const getIdChecks = state => fromChecks.getIdChecks(state.checks);
