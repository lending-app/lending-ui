import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';
import reducers from '../reducers';
import { getUser } from '../actions/auth';
import * as api from '../api';
import { loadAuthState } from '../localStorage';

const configureStore = () => {
  const middlewares = [thunk];
  const persistedState = loadAuthState();

  if (process.env.NODE_ENV !== 'production') {
    middlewares.push(createLogger());
  }

  const store = createStore(
    reducers,
    applyMiddleware(...middlewares),
  );

  if (persistedState && persistedState.auth.isAuthenticated) {
    api.setApiAuthHeader(persistedState.auth.authToken);
    store.dispatch(getUser(persistedState.auth.userInfo._id, persistedState.auth.authToken));
  }

  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('../reducers', () => {
      /* eslint-disable-next-line global-require */
      const nextRootReducer = require('../reducers');
      store.replaceReducer(nextRootReducer);
    });
  }

  return store;
};

export default configureStore;
